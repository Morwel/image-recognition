'use strict';

var fs     = require('fs');
var path   = require('path');
var crypto = require('crypto');

const ROOT_PATH = path.normalize(__dirname);

exports.save = function(req, res, next){
  console.log("file", req.file, req.files);

  var file = req.body.FileName,
      path = './photos/storage/';

  // Logic for handling missing file, wrong mimetype, no buffer, etc.

  var buffer = decodeBase64Image(file); //Note: buffer only populates if you set inMemory: true.
  var fileName = getUID();
  var stream = fs.createWriteStream(path + fileName + '.jpg');

  stream.write(buffer);

  stream.on('error', function(err) {
    console.log('Could not write file to memory.');
    res.status(400).send({
      message: 'Problem saving the file. Please try again.'
    });
  });

  stream.on('finish', function() {
    console.log('File saved successfully.');
    var data = {
      message: 'File saved successfully.'
    };

    res.jsonp(data);
  });

  stream.end();

  console.log('Stream ended.');
};

exports.get = function(req, res, next) {
  var photoStoragePath = ROOT_PATH + path.normalize('/storage');

  fs.readdir(photoStoragePath, function(err, files) {
    if(err) {
      console.log(err);
      next(err);
    }

    files = files.filter(function(file) {
      return /jpg/.test(file);
    });

    var fstats = [];

    files.forEach(function(file) {
      fstats.push(getFileProp(photoStoragePath + '/' + file));
    });

    Promise.all(fstats)
      .then(function(fstatsResponses) {

        var filesFinal = [];

        files.forEach(function(fileName, i) {
          filesFinal.push({
            name: fileName,
            mtime: fstatsResponses[i].mtime.getTime()
          });
        });

        filesFinal.sort(function(a, b) {

          var m1 = a.mtime;
          var m2 = b.mtime;

          return m1 > m2 ? -1 : (m1 < m2 ? 1 : 0);

        });

        res.type('json');
        res.send(filesFinal);
      });


  });

  // Nice to have:
  // 1 - params - how many photos.
  // 2 - Sort by date
};

function getUID(){
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    .replace(/[xy]/g, function(c) {
      var r = crypto.randomBytes(1)[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
}

function decodeBase64Image(dataString){
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  var response = {};

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');

  return response.data;
}

function getFileProp(filePath, prop) {
  var defer = new Promise(function(resolve, reject) {
    fs.stat(filePath, function(err, fstat) {
      if(err) {
        console.log('error:', err);
        reject({ error: err });
      }
      resolve(prop !== undefined ? fstat[prop] : fstat);
    });
  });

  return defer;
}
