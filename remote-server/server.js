'use strict';

var path = require('path');
var rootPath = path.normalize(process.cwd());
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var multer  = require('multer');
var upload = multer({
  inMemory: true //This is important. It's what populates the buffer
});

var cors = require('cors');
var whitelist = ['http://localhost:5000', 'http://127.0.0.1:5000'];
var corsOptionsDelegate = function(req, callback){
  var corsOptions;
  console.log('========> ', req.header('Origin'));

  if(whitelist.indexOf(req.header('Origin')) !== -1){
    corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
  }else{
    corsOptions = { origin: false }; // disable CORS for this request
  }
  
  callback(null, corsOptions); // callback expects two parameters: error and options
};

var express = require('express');
var app = express();
var port = Number(process.env.PORT) || process.argv[2] || 5000;

// Connect to mongodb
var connect = function () {
  var mongoURI = 'mongodb://mm-party-photos:glow@ds061374.mongolab.com:61374/mm-party-photos';
  var options = { server: { socketOptions: { keepAlive: 1 } } };
  mongoose.connect(mongoURI, options);
  require('./photos/model');
};

connect();

let photoController = require('./photos');

app.use(express.static(rootPath + '/photos/storage'));

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

// viewed at http://localhost:$PORT
app.get('/', cors(), function(req, res){
  res.send('Noo, there is nothing here!');
});

app.get('/photos/:id', cors(), photoController.get);
app.get('/photos', cors(corsOptionsDelegate), photoController.list);
app.post('/photos', cors(corsOptionsDelegate), upload.single('snapshot'), photoController.save);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log(req.path);
  var err = new Error('Not Found');
  err.status = 404;
});

app.listen(port);

console.log("Server listens on port", port);














