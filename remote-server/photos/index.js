'use strict';

var fs     = require('fs');
var path   = require('path');
var crypto = require('crypto');

var mongoose = require('mongoose');
var Photo = mongoose.model('Photo');

exports.save = function(req, res, next){ 
  var photo = new Photo();
  photo.data = req.body.FileName;
  photo.fileName = generateFileName();
  
  photo.save(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(400);
    } else {
      console.log('Saved!');
      res.send({ message: "The photo is saved successfully!"});
    }
  });
};

exports.list = function(req, res, next){
  Photo.list({}, function (err, photos) {
    if (err) { return res.sendStatus(400); }
    var listIDs = [];
    photos.forEach(function(p){
      listIDs.push(p._id);
    })
    res.send(listIDs);
  });
  
  // Nice to have:
  // 1 - params - how many photos.
  // 2 - Sort by date
};

exports.get = function(req, res, next){
  var id = req.params.id;
  Photo.load({_id: id}, function(err, photo){
    var image = decodeBase64Image(photo.data);
    res.contentType(image.type);
    res.send(image.data);
  });
};

function getRandomStuff(){
  return 'xxxxxx'
    .replace(/[xy]/g, function(c) {
      var r = crypto.randomBytes(1)[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
}

function generateFileName(){
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var date = new Date();
  var hour = (date.getHours > 9? '' : '0') + date.getHours();
  var mins = (date.getMinutes > 9? '' : '0') + date.getMinutes();
  var dateTime = date.getDate()+'-'+(months[date.getMonth()])+'-'+date.getFullYear()+'|'+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();

  return 'MM-Party' + '|' + dateTime + '|' + getRandomStuff();
}

function decodeBase64Image(dataString){
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  var response = {};

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');

  return response;
}
