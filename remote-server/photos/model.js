var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PhotoSchema = new Schema({
  fileName: String,
  data: String, //base64
  _createdAt: {type : Date, default : Date.now}
});

PhotoSchema.statics = {
  load: function (id, cb) {
    this
      .findOne({ _id : id })
      .exec(cb);
  },

  list: function (options, cb) {
    var criteria = options.criteria || {};

    this
      .find(criteria)
      .sort({'_createdAt': -1}) // sort by date
      // .limit(options.perPage)
      // .skip(options.perPage * options.page)
      .exec(cb);
  }
};

mongoose.model('Photo', PhotoSchema);
