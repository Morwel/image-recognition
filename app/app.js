(function () {
  'use strict';

  window.onload = init;

  // var BASE_URL = 'http://mm-photobooth.herokuapp.com';
  var BASE_URL = 'http://' + window.location.host;

  function init() {

      //Selectors
      var video = document.getElementById('webcam'),
          canvas = document.getElementById('canvas'),
          imgCollection = document.getElementById('image-collection'),
          shotTimer = document.getElementById('timer');

      var imgData;

      var clock;
      var started = false;
      var timer = 4;

      var attempts = 0;

      var stat = new profiler();

      var ctx, canvasWidth, canvasHeight;
      var img_u8, work_canvas, work_ctx;

      var max_work_size = 160;
      //
      //image-collection
      new Miniscroll(document.querySelectorAll('#image-collection')[0], {
          axis: "y",
          size: 6,
          sizethumb: "auto"
        });

      // lets do some fun
      try {
          video.addEventListener('loadeddata', readyListener);

          compatibility.getUserMedia({video: true}, function(stream) {
              try {
                  video.src = compatibility.URL.createObjectURL(stream);
              } catch (error) {
                  video.src = stream;
              }
              setTimeout(function() {
                      video.play();
                  }, 500);
          }, function (error) {

            console.error(error);
              // $('#canvas').hide();
              // $('#log').hide();
              // $('#no_rtc').html('<h4>WebRTC not available.</h4>');
              // $('#no_rtc').show();
          });
      } catch (error) {
        alert(error);
          // $('#canvas').hide();
          // $('#log').hide();
          // $('#no_rtc').html('<h4>Something goes wrong...</h4>');
          // $('#no_rtc').show();
      }

      function readyListener(event) {
        findVideoSize();
      }

      function findVideoSize() {
        console.log('[findVideoSize]', video);
        if(video.videoWidth > 0 && video.videoHeight > 0) {
            video.removeEventListener('loadeddata', readyListener);
            onDimensionsReady(video.videoWidth, video.videoHeight);
        } else {
            if(attempts < 10) {
                attempts++;
                setTimeout(findVideoSize, 200);
            } else {
                onDimensionsReady(800, 600);
            }
        }
      }

      function onDimensionsReady(width, height) {
        console.log('[onDimensionsReady]', width, height);
        getAllPhotos();
        demo_app(width, height);
        compatibility.requestAnimationFrame(tick);
      }

      function getAllPhotos() {
        var photoArray = [];

        var options = {
          url: BASE_URL + '/photos',
          type: 'GET',
          success: function (data) {

            data.forEach(function (img) {
              addPhoto(BASE_URL + '/' + img);
            });
          },
          error: function (error) {
            console.log(error);
          }
        };

        $.ajax(options);
      }

      function demo_app(videoWidth, videoHeight) {
          console.log('[demo_app]', videoWidth, videoHeight);
          canvasWidth  = canvas.width;
          canvasHeight = canvas.height;
          ctx = canvas.getContext('2d');

          ctx.fillStyle = "rgb(133,56,255)";
          ctx.strokeStyle = "rgb(133,56,255)";

          // var scale = Math.min(max_work_size/videoWidth, max_work_size/videoHeight);
          var scale = 1;

          var w = (videoWidth*scale);//|0;
          var h = (videoHeight*scale);//|0;

          img_u8 = new jsfeat.matrix_t(w, h, jsfeat.U8_t | jsfeat.C1_t);
          work_canvas = document.createElement('canvas');

          console.log('[demo_app]', w, h);
          work_canvas.width = w;
          work_canvas.height = h;
          work_ctx = work_canvas.getContext('2d');

          jsfeat.bbf.prepare_cascade(jsfeat.bbf.face_cascade);

          stat.add("bbf detector");
      }

      function tick() {
          compatibility.requestAnimationFrame(tick);
          stat.new_frame();
          if (video.readyState === video.HAVE_ENOUGH_DATA) {

              ctx.drawImage(video, 0, 0, canvasWidth, canvasHeight);

              work_ctx.drawImage(video, 0, 0, work_canvas.width, work_canvas.height);
              var imageData = work_ctx.getImageData(0, 0, work_canvas.width, work_canvas.height);

              stat.start("bbf detector");

              jsfeat.imgproc.grayscale(imageData.data, work_canvas.width, work_canvas.height, img_u8);

              // possible options
              //jsfeat.imgproc.equalize_histogram(img_u8, img_u8);

              var pyr = jsfeat.bbf.build_pyramid(img_u8, 24*2, 24*2, 4);

              var rects = jsfeat.bbf.detect(pyr, jsfeat.bbf.face_cascade);
              rects = jsfeat.bbf.group_rectangles(rects, 1);

              stat.stop("bbf detector");
              //console.log(rects.length);

              if(rects.length > 0) {
                startTimer();
              }
              else {
                stopTimer();
              }
              // draw only most confident one
              draw_faces(ctx, rects, canvasWidth/img_u8.cols, 2);

              //Hide logging for test
              //log.innerHTML = stat.log();
          }
      }

      function startTimer() {
        if(!started) {
          clock = setInterval(incrementer, 1000);
          started = true;
        }
      }

      function stopTimer() {
        if(clock) {
          window.clearInterval(clock);
        }

        timer = 4;
        started = false;
        shotTimer.innerHTML = '';
      }

      function incrementer() {
        timer--;
        shotTimer.innerHTML = timer;

        switch (timer) {
          case 1:
            shotTimer.className = "one";
            break;
          case 2:
            shotTimer.className = "two";
            break;
          case 3:
            shotTimer.className = "three";
            break;
          default:
            shotTimer.className = "";
        }
        if(timer < 1) {
          shotTimer.innerHTML = "!! SHOT !!";
          doScreenShot();
          stopTimer();
        }
      }

      function doScreenShot() {
        var photoData = work_canvas.toDataURL('image/jpeg', 1);
        console.log('photoData:', photoData.length, photoData);
        // document.querySelectorAll('.theImage')[0].src = photoData;
        sendPhoto(photoData);
      }

      function sendPhoto(photoDataUrl) {

        var fd = new FormData();

        fd.append("FileName", photoDataUrl);

        var options = {
          url: BASE_URL + '/photos',
          type: 'POST',
          data: fd,
          cache: false,
          contentType: false,
          processData: false,
          success: function () {
            addPhoto(photoDataUrl);
          },
          error: function (error) {
            console.log(error);
          }
        };

        $.ajax(options);
      }

      function addPhoto(photoDataUrl) {
        var newImg = document.createElement('img');

        newImg.className = 'snapshot';
        newImg.src = photoDataUrl;
        var image = document.querySelector('#image-collection img');
        imgCollection.insertBefore(newImg, image);
      }

      function draw_faces(ctx, rects, sc, max) {
          var on = rects.length;
          // console.log(ctx, rects, sc, max);
          if(on && max) {
            jsfeat.math.qsort(rects, 0, on-1, function(a,b){return (b.confidence<a.confidence);})
          }
          var n = max || on;
          n = Math.min(n, on);
          var r;
          for(var i = 0; i < n; ++i) {
              r = rects[i];
              ctx.strokeRect((r.x*sc)|0,(r.y*sc)|0,(r.width*sc)|0,(r.height*sc)|0);
          }
      }

      window.unload = function() {
          video.pause();
          video.src=null;
      };

      setInterval(function() {
        window.location.reload();
      }, 1000 * 60 * 3);
  }
})();
