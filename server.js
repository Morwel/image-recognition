'use strict';
{
  let path = require('path');
  let rootPath = path.normalize(process.cwd());
  let leMorgan = require('morgan');
  var bodyParser = require('body-parser');

  let express = require('express');
  let app = express();

  var multer  = require('multer');
  var upload = multer({
    dest: './photos/storage/',
    limits: {
      fieldNameSize: 50,
      files: 1,
      fields: 5,
      fileSize: 1024 * 1024
    },
    rename: function(fieldname, filename) {
      return filename;
    },
    onFileUploadStart: function(file) {
      console.log('Starting file upload process.');
      if(file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
        return false;
      }
    },
    inMemory: true //This is important. It's what populates the buffer.
  });

  let photoController = require('./photos');

  app.use(leMorgan('short'));

  app.use('/', express.static('stream', { index: 'stream.html' }));

  app.use(express.static(rootPath + '/bower_components'));
  app.use(express.static(rootPath + '/app'));
  app.use(express.static(rootPath + '/photos/storage'));

  app.use(bodyParser.json({limit: '5mb'}));
  app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

  // viewed at http://localhost:8080
  app.get('/', function(req, res) {
    res.sendFile(path.join(rootPath + '/index.html'));
  });

  app.post('/photos', upload.single('snapshot'), photoController.save);
  app.get('/photos', photoController.get);

  // catch 404 and forward to error handler
  // app.use(function(req, res, next) {
  //   console.log(req.path);
  //   var err = new Error('Not Found');
  //   err.status = 404;
  //   next(err);
  // });

  app.listen(5000);

  console.log("Server listens on port 5000");
}

